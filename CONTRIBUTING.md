# CONTRIBUTION GUIDE TO ESRH TP HL7 PROJECT

Contributions are always welcome on this project. If you want to lend us a hand, be sure to follow these little rules for a good merge request.

## Create your own branch

Be sure to create your own branch for your work, based on the up-to-date `develop` branch of this project. Follow the guideline of the [gitflow-avh](https://github.com/petervanderdoes/gitflow-avh) method.

    git checkout develop
    git checkout -b feature/my-new-awesome-feature
